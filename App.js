import React, { useEffect } from 'react';
import { Text, View, LogBox, StatusBar, SafeAreaView, Image } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';

import Navigator from './src/navigator';

import SplashScreen from 'react-native-splash-screen';


LogBox.ignoreAllLogs(true);

const App = () => {

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hide()
        }, 1000);
    }, [])
    
    return (
        <NavigationContainer>
            <StatusBar backgroundColor = "#0D0D0D"   />  
            <Navigator />
        </NavigationContainer>
    )
}

export default App
