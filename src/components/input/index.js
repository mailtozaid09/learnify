import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, useColorScheme, } from 'react-native';

import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { Poppins } from '../../global/fontFamily';


const Input = ({ placeholder, isSearch, label, error, value, onClearSearchText, onChangeText, onChangeEyeIcon, isPassword, showEyeIcon }) => {

    return (
        <View style={{marginBottom: 12,}} >

            <View style={[styles.inputContainer]}  >

                <View>
                    <Text style={styles.countryCode} >+91</Text>
                </View>

                <TextInput
                    placeholder={placeholder}
                    style={styles.inputStyle}
                    value={value}
                    maxLength={10}
                    keyboardType='number-pad'
                    onChangeText={onChangeText}
                    placeholderTextColor={colors.gray}
                    secureTextEntry={isPassword  && !showEyeIcon ? true : false}
                />

                
            </View>
            {error && <Text style={styles.error} >{error}</Text>}
        </View>
    )
}


const styles = StyleSheet.create({
    inputContainer: {
        height: 60,
        paddingHorizontal: 15,
        paddingVertical: 4,
        borderWidth: 0.5,
        borderColor: colors.gray,
        borderRadius: 30,
        width: screenWidth-40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    inputStyle: {
        fontSize: 16,
        alignItems: 'center',
        justifyContent: 'center',
        fontFamily: Poppins.Medium,
        color: colors.black,
        flex: 1,
    },
    iconImage: {
        height: 24,
        width: 24
    },
    countryCode: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black,
        marginRight: 10,
    },
    error: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.reddish,
        marginTop: 4,
    },

})

export default Input
