import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'

import PrimaryButton from '../../components/button/PrimaryButton'
import LottieView from 'lottie-react-native'


const HomeScreen = (props) => {

    useEffect(() => {
        const backAction = () => {
            BackHandler.exitApp()
            return true;
        };
    
        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
    
        return () => backHandler.remove();
      }, []);
    

    const logoutFunction = () => {
        props.navigation.navigate('LoginStack', {screen: 'Onboarding'})
    }
    
    return (
        <SafeAreaView style={styles.container} >
           
                <View style={styles.mainContainer} >
                    <View style={{}} >
                        <LottieView
                            source={media.hi_lottie} 
                            autoPlay 
                            loop 
                            style={{height: 200, width: 200,  }}
                        />
                    </View>
                    <View style={{marginTop: -40}} >
                        <LottieView
                            source={media.hitext_lottie} 
                            autoPlay 
                            loop 
                            style={{height: 200, width: 200,  }}
                        />
                    </View>
                
                    <View>
                        <PrimaryButton
                            title="LOGOUT" 
                            onPress={() => {
                                logoutFunction()
                            }}
                        />
                    </View>
                </View>
          
           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
})


export default HomeScreen