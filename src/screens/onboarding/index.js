import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'

import PrimaryButton from '../../components/button/PrimaryButton'




const OnboardingScreen = ({navigation}) => {

    
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.imgContainer} >
                <Image source={media.getstarted} style={{height: screenWidth, width: screenWidth, resizeMode: 'contain', marginTop: 20}} />
                <Text style={styles.subtitle} >Ignite Your Learning Journey with Learnify{'\n'} – Your Learning Companion.</Text>
            </View>
            <PrimaryButton
                title="Get Started" 
                onPress={() => {
                    navigation.navigate('Login');
                }}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: colors.white,
    },
    imgContainer: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    subtitle: {
        fontSize: 18,
        paddingHorizontal: 20,
        fontFamily: Poppins.Medium,
        color: colors.black,
        textAlign: 'center'
    }
})

export default OnboardingScreen