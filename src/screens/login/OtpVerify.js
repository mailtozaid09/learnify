import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, StyleSheet, Button, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, TextInput, Alert, } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import { screenWidth } from '../../global/constants'

import PrimaryButton from '../../components/button/PrimaryButton'

import LottieView from 'lottie-react-native'
import OTPTextView from 'react-native-otp-textinput';

const OtpVerify = (props) => {

    const input = useRef(null);

    const [errors, setErrors] = useState({});
    const [otpValue, setOtpValue] = useState('');

    const [number, setNumber] = useState('');
    const [orgNumber, setOrgNumber] = useState('');

    useEffect(() => {
        
        var num = '+91 ' + props?.route?.params?.params
        setNumber(num)
        setOrgNumber(props?.route?.params?.params)
    }, [])

    const verifyFunction = () => {
        
        var isValid = true
                
        if(otpValue.length != 6){
            console.log("Please enter a valid OTP");
            isValid = false
            setErrors((prev) => {
                return {...prev, OTP: 'Please enter a valid OTP'}
            })
        }

        if(isValid){
            props.navigation.navigate('HomeStack', {params: 'mobileNumber'});
        }


    }
    
    return (
        <SafeAreaView style={styles.container} >
            <ScrollView keyboardShouldPersistTaps >
                <View style={styles.mainContainer} >
                    <View>
                        <LottieView
                            source={media.otp} 
                            autoPlay 
                            loop 
                            style={{height: screenWidth-70, width: screenWidth-70, }}
                        />
                    </View>

                    <View style={{marginVertical: 20,  alignItems: 'center'}} >
                        <Text style={styles.title} >OTP sent to</Text>
                        <Text style={styles.subtitle} >{number}</Text>
                    </View>
                
                    <View>

                        <View style={styles.otpContainer} >
                            <OTPTextView
                                ref={input}
                                containerStyle={styles.textInputContainer}
                                handleTextChange={(otp) => {setOtpValue(otp); setErrors({})}}
                                inputCount={6}
                                tintColor={colors.primary}
                                keyboardType="numeric"
                                textInputStyle={styles.textInputStyle}
                            />
                        
                            {errors.OTP && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: colors.reddish }} >{errors?.OTP}</Text>}
                        </View>

                        <PrimaryButton
                            title="Verify OTP" 
                            onPress={() => {
                                verifyFunction()
                            }}
                        />
                        <View style={styles.alreadyContainer} >
                            <Text style={styles.alreadyText} >By signing up, you agree with our Terms and conditions</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    mainContainer: {
        padding: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    alreadyContainer: {
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    alreadyText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.gray,
        marginTop: 8,
        textAlign: 'center',
    },
    title: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    subtitle: {
        fontSize: 16,
        lineHeight: 19,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    otpContainer: {
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-between'
    },
    textInputStyle: {
        height: 44,
        width: 44,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: colors.black,
    }
})

export default OtpVerify