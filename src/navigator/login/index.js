import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { colors } from '../../global/colors';
import { Poppins } from '../../global/fontFamily';

import LoginScreen from '../../screens/login';
import OtpVerify from '../../screens/login/OtpVerify';
import OnboardingScreen from '../../screens/onboarding';


const Stack = createStackNavigator();

const LoginStack = ({navgation}) => {

    return (
        <Stack.Navigator 
            initialRouteName="Onboarding" 
        >
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Log In',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="OtpVerify"
                component={OtpVerify}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'OTP Verify',
                    headerTitleAlign: 'center',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="Onboarding"
                component={OnboardingScreen}
                options={{
                    headerShown: false,
                    headerLeft: () => null,
                    headerTitle: 'Onboarding',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: colors.black,
    },
    headerStyle: {
        //backgroundColor: colors.primary,
    }
})

export default LoginStack