import React, {useState, useEffect} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';


import { useNavigation } from '@react-navigation/native';

import HomeStack from './home';
import LoginStack from './login';

import AsyncStorage from '@react-native-async-storage/async-storage';


const Stack = createStackNavigator();


const Navigator = ({}) => {

    const navigation = useNavigation()

    const [userDetails, setUserDetails] = useState(null);

  

    useEffect(() => {
        //getUserDetails()
    }, [])
    

    const getUserDetails = async () => {
        let user = await AsyncStorage.getItem('user_details');  
        let user_details = JSON.parse(user);  
        setUserDetails(user_details)
    }
   


    return (
        <>
        <Stack.Navigator 
        initialRouteName={userDetails ? 'HomeStack' : 'LoginStack'}  
        >
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="HomeStack"
                component={HomeStack}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
        
        </>
    );
}

export default Navigator